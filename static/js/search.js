if (document.querySelector('#SearchBox')) {
	const search = instantsearch({
		appId: '0SL7CJF1A1',
		apiKey: '90a8fcd2b577aca0352fec502988d782',
		indexName: 'joblistberlin',
		routing: true,
		searchFunction(helper) {
			const search = document.querySelector('#SearchHits')
			const searchPagination = document.querySelector('#SearchPagination')
			search.style.display = helper.state.query === '' ? 'none' : ''
			searchPagination.style.display = helper.state.query === '' ? 'none' : ''
			helper.search()
		}
	})

	// initialize SearchBox
	search.addWidget(
		instantsearch.widgets.searchBox({
			container: '#SearchBox',
			placeholder: 'Search for companies and tags',
			showReset: false
		})
	)

	// initialize hits widget
	search.addWidget(
		instantsearch.widgets.hits({
			container: '#SearchHits',
			templates: {
				empty: 'No results',
				item: '<list-item type="{{type}}" direction="row"><a href="{{permalink}}">{{title}}</a></list-item>'
			}
		})
	)

	search.addWidget(
		instantsearch.widgets.pagination({
			container: '#SearchPagination',
			showFirst: false
		})
	)

	search.start()
}

export default {}
